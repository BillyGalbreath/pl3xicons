<?php
$get = @$_GET["get"];

if ($get == "all") {
	header("Cache-Control: no-cache");
	require_once("spyc.php");
	$icons = spyc_load_file("icons.yml");

	$total = count($icons);
	$i = 0;
	foreach ($icons as $key => $value) {
        $i++;
        if ($value === "__UNUSED__") {
                continue;
        }
        echo $key . ":" . $value;
        if ($i !== $total) {
                echo ",";
        }
	}
	die();
}
?><!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Pl3xIcons Translator</title>
	<style rel="stylesheet">
.holder {
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
	background-color: red;
	width: 1000px;
	height: 500px;
}

.left {
    -webkit-box-ordinal-group: 1;
    -moz-box-ordinal-group: 1;
    -ms-flex-order: 1;
    -webkit-order: 1;
    order: 1;
	width: 250px;
	background-color: #dedede;
}

.right {
	-webkit-box-flex: 1;
    -moz-box-flex: 1;
    -webkit-flex: 1;
    -ms-flex: 1;
    flex: 1;
    -webkit-box-ordinal-group: 2;
    -moz-box-ordinal-group: 2;
    -ms-flex-order: 2;
    -webkit-order: 2;
    order: 2;
	background-color: #dedede;
}

textarea {
	resize: none;
	display: block;
	margin: 15px auto;
	width: 220px;
	height: 200px;
}

.buttons {
	text-align: center;
}

button {
	width: 110px;
}

.icons {
	list-style-type: none;
	margin: 5px 5px 5px 0px;
	padding: 0px;
	font-size: 0px;
	overflow-x: hidden;
	overflow-y: scroll;
	height: 450px;
	border-top: 1px solid #fff;
	border-left: 1px solid #fff;
	border-right: 1px solid #888;
	border-bottom: 1px solid #888;
}

.icons li {
	display: inline-block;
	padding: 0px;
	margin: 0px;
	border: 1px solid #dedede;
}
.icons li:hover {
	background-color: red;
	border: 1px solid #800;
}
.icons li div {
	width: 16px;
	height: 16px;
	background-size: 256px 256px;
}
.icons li div.big {
	width: 32px !important;
	height: 32px !important;
	background-size: 512px 512px !important;
}

.status {
	margin: 10px 10px 10px 0px;
	padding: 0px 10px;
	border-top: 1px solid #888;
	border-left: 1px solid #888;
	border-right: 1px solid #fff;
	border-bottom: 1px solid #fff;
	background-color: #dedede;
	height: 17px;
	line-height: 18px;
	font-size: 12px;
	
}
	</style>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
$(document).ready(function(){
	$(document).tooltip();

	$big = $_GET("big") == "true" ? true : false;
	
	$.ajax({
		url: "index.php?get=all",
		data: null,
		dataType: "text",
		success: function($data, $status) {
			var $unordered = {};

			var $data = $data.split(",");
			$.each($data, function() {
				var $vars = this.split(":");
				$icon = $("<li></li>");
				$icon.attr("icon-id", $vars[0]);
				$icon.attr("icon-name", $vars[1]);
				$icon.attr("title", $vars[1]);
				$unordered[$vars[0]] = $icon;
			});

			Object.keys($unordered).sort().forEach(function($key) {
				$(".icons").append($unordered[$key]);
			});

			$("li")
				.each(function() {
					var $hex = $(this).attr("icon-id");
					var $page = $hex.substring(0, 2);
					var $id = parseInt($hex.substring(2), 16);
					var $row = Math.floor(($id % 256) / 16);
					var $col = $id % 16;
					var $x = ($big ? 32 : 16) * $col;
					var $y = ($big ? 32 : 16) * $row;
					var $div = $("<div></div>");
					$div.css("background-image", "url(assets/minecraft/textures/font/unicode_page_" + $page + ".png)");
					$div.css("background-position", "-" + $x + "px -" + $y + "px");
					if ($big) {
						$div.attr("class", "big");
					}
					$(this).append($div);
				})
				.click(function() {
					$("#input").val($("#input").val() + "{" + $(this).attr("icon-name") + "}");
				});
			
			$(".status").html("Done.");
		
		}
	});
	
	$("#translate").click(function() {
		$text = $("#input").val();
		$("li").each(function() {
			$text = $text.split("{" + $(this).attr("icon-name") + "}").join($_CHAR($(this).attr("icon-id")));
		});
		$text = $text.replace(/&([a-f0-9k-or])/gi, '\u00a7$1');
		$("#output").val($text);
	});
		
	$("#decode").click(function() {
		$text = $("#input").val();
		$("li").each(function() {
			$text = $text.split($_CHAR($(this).attr("icon-id"))).join("{" + $(this).attr("icon-name") + "}");
		});
		$text = $text.replace(/\u00a7([a-f0-9k-or])/gi, '&$1');
		$("#output").val($text);
	});
});

function $_CHAR(s) {
	return String.fromCharCode(parseInt(s, 16));
}

function $_GET(q,s) {
    s = (s) ? s : window.location.search;
    var re = new RegExp('&amp;'+q+'=([^&amp;]*)','i');
    return (s=s.replace(/^\?/,'&amp;').match(re)) ?s=s[1] :s='';
}
	</script>
  </head>
  <body>
    <h1>Pl3xIcons Translator</h1>
	<div class="holder">
	  <div class="left">
	    <textarea id="input" name="input"></textarea>
		<div class="buttons">
		  <button id="translate">Translate</button>
		  <button id="decode">Decode</button>
		</div>
		<textarea id="output" name="output" readonly></textarea>
	  </div>
	  <div class="right">
	    <ul class="icons">
		</ul>
		<div class="status">Loading . . .</div>
	  </div>
	</div>
  </body>
</html>
