package net.pl3x.bukkit.pl3xicons;

import net.pl3x.bukkit.pl3xicons.api.IconManager;
import net.pl3x.bukkit.pl3xicons.command.CmdIcon;
import net.pl3x.bukkit.pl3xicons.configuration.Config;
import net.pl3x.bukkit.pl3xicons.configuration.EmoticonConfig;
import net.pl3x.bukkit.pl3xicons.configuration.Lang;
import net.pl3x.bukkit.pl3xicons.listener.APIListener;
import net.pl3x.bukkit.pl3xicons.listener.BukkitListener;
import net.pl3x.bukkit.pl3xicons.listener.ResourcePackListener;
import net.pl3x.bukkit.pl3xicons.manager.Pl3xIconManager;
import net.pl3x.bukkit.pl3xicons.manager.ProtocolLibManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Pl3xIcons extends JavaPlugin {
    private static ProtocolLibManager protocolLibManager;

    public void onEnable() {
        Config.reload();
        Lang.reload();
        EmoticonConfig.getConfig();
        EmoticonConfig.reload();

        extractZipFiles();

        doProtocolLibCheck();

        IconManager.setManager(new Pl3xIconManager());
        IconManager.getManager().reloadIconConfig();

        Bukkit.getPluginManager().registerEvents(new APIListener(), this);
        Bukkit.getPluginManager().registerEvents(new BukkitListener(), this);

        getCommand("pl3xicons").setExecutor(new CmdIcon(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        Logger.info(getName() + " Disabled.");
    }

    public static ProtocolLibManager getProtocolLibManager() {
        if (protocolLibManager == null) {
            protocolLibManager = new ProtocolLibManager();
        }
        return protocolLibManager;
    }

    public static Pl3xIcons getPlugin() {
        return Pl3xIcons.getPlugin(Pl3xIcons.class);
    }

    /**
     * Gets the IconManager instance
     *
     * @return IconManager instance
     * @deprecated Use {@link IconManager#getManager()} instead
     */
    public IconManager getIconManager() {
        return IconManager.getManager();
    }

    private void doProtocolLibCheck() {
        if (Bukkit.getPluginManager().isPluginEnabled("ProtocolLib")) {
            Logger.info("ProtocolLib found. Registering packet listener...");
            Bukkit.getPluginManager().registerEvents(new ResourcePackListener(this), this);
        } else {
            Logger.warn("# ProtocolLib NOT found and/or enabled!");
            Logger.warn("# It is recommended to install ProtocolLib to gain better control over sending/detecting the icon pack to your users.");
            Logger.warn("# https://www.spigotmc.org/resources/protocollib.1997/");
            Logger.warn("# Falling back to legacy behavior...");
        }
    }

    private void extractZipFiles() {
        String fileName = "Pl3xIconsPack.zip";
        if (!new File(getDataFolder(), fileName).exists()) {
            Logger.info("&3Extracting " + fileName + " to plugin directory.");
            try {
                InputStream in = this.getResource(fileName);
                OutputStream out = new FileOutputStream(new File(getDataFolder(), fileName));

                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf, 0, buf.length)) != -1) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
