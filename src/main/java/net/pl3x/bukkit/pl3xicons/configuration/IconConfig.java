package net.pl3x.bukkit.pl3xicons.configuration;

import net.pl3x.bukkit.pl3xicons.Pl3xIcons;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class IconConfig {
    private static File configFile;
    private static FileConfiguration config;

    public static FileConfiguration getConfig() {
        if (config != null) {
            return config;
        }
        configFile = new File(Pl3xIcons.getPlugin().getDataFolder(), "icons.yml");
        saveDefault();
        reload();
        return config;
    }
    
    public static void reloadConfig() {
        config = null;
        getConfig();
    }

    private static void reload() {
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private static void saveDefault() {
        if (!configFile.exists()) {
            Pl3xIcons.getPlugin().saveResource("icons.yml", false);
        }
    }
}
