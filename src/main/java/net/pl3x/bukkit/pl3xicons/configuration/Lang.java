package net.pl3x.bukkit.pl3xicons.configuration;

import net.pl3x.bukkit.pl3xicons.Pl3xIcons;
import net.pl3x.bukkit.pl3xicons.api.IconManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Lang {
    public static String ERROR_PLAYER_COMMAND = "&4This command is only available to players.";
    public static String ERROR_COMMAND_NO_PERMISSION = "&4You do not have permission for that command!";
    public static String ERROR_INVALID_COMMAND_STRUCTURE = "&4Invalid command structure!";
    public static String ERROR_NOT_VALID_ICON_CODE = "&4Not a valid icon code.";
    public static String ERROR_SENDING_RESOURCE_PACK = "&4Error sending resource pack!";
    public static String LIST_INVALID_PAGE_NUMBER = "&4Invalid page number.";
    public static String LIST_PAGE_TITLE = "&dIcons &e- &3Page &e{page}&7/&e{total}";
    public static String LIST_ICON_ENTRY = "&f{icon} &7-> &3{&e{code}&3}";
    public static String RESOURCE_PACK_ACCEPTED = "&dLoading icon pack. Please wait...";
    public static String RESOURCE_PACK_DECLINED = "&6You have declined the icon pack. You will not see any icons until you accept the icon pack. Your server resource pack settings must be set to prompt or allow to install.";
    public static String RESOURCE_PACK_DOWNLOAD_FAILED = "&4The icon pack has failed to download.";
    public static String RESOURCE_PACK_LOADED = "&f{diamond-sword} &dIcon pack loaded. &f{diamond-sword}";
    public static String FORCE_KICK_MESSAGE = "&4Icon Pack {action}\n&6The icon pack is required for this server";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    public static void reload() {
        Pl3xIcons plugin = Pl3xIcons.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        ERROR_PLAYER_COMMAND = config.getString("error-player-command", "&4This command is only available to players.");
        ERROR_COMMAND_NO_PERMISSION = config.getString("error-command-no-permission", "&4You do not have permission for that command!");
        ERROR_INVALID_COMMAND_STRUCTURE = config.getString("error-invalid-command-structure", "&4Invalid command structure!");
        ERROR_NOT_VALID_ICON_CODE = config.getString("error-not-valid-icon-code", "&4Not a valid icon code.");
        ERROR_SENDING_RESOURCE_PACK = config.getString("error-sending-resource-pack", "&4Error sending resource pack!");
        LIST_INVALID_PAGE_NUMBER = config.getString("list-invalid-page-number", "&4Invalid page number.");
        LIST_PAGE_TITLE = config.getString("list-page-title", "&dIcons &e- &3Page &e{page}&7/&e{total}");
        LIST_ICON_ENTRY = config.getString("list-icon-page", "&f{icon} &7-> &3{&e{code}&3}");
        RESOURCE_PACK_ACCEPTED = config.getString("resource-pack-accepted", "&dLoading icon pack. Please wait...");
        RESOURCE_PACK_DECLINED = config.getString("resource-pack-declined", "&6You have declined the icon pack. You will not see any icons until you accept the icon pack. Your server resource pack settings must be set to prompt or allow to install.");
        RESOURCE_PACK_DOWNLOAD_FAILED = config.getString("resource-pack-download-failed", "&4The icon pack has failed to download.");
        RESOURCE_PACK_LOADED = config.getString("resource-pack-loaded", "&f{diamond-sword} &dIcon pack loaded. &f{diamond-sword}");
        FORCE_KICK_MESSAGE = config.getString("force-kick-message", "&4Icon Pack {action}\n&6The icon pack is required for this server");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(IconManager.getManager().translate(part));
        }
    }
}
