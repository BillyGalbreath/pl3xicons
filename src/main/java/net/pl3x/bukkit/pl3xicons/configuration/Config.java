package net.pl3x.bukkit.pl3xicons.configuration;

import net.pl3x.bukkit.pl3xicons.Pl3xIcons;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static boolean NOTIFICATIONS = true;
    public static boolean SEND_RESOURCE_PACK = true;
    public static boolean FORCE = false;
    public static String ICON_PACK = "http://my.server.com/pl3xiconspack.zip";
    public static int ICONS_PER_PAGE = 9;

    public static void reload() {
        Pl3xIcons plugin = Pl3xIcons.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        NOTIFICATIONS = config.getBoolean("notifications", true);
        SEND_RESOURCE_PACK = config.getBoolean("send-resource-pack", true);
        FORCE = config.getBoolean("force", false);
        ICON_PACK = config.getString("icon-pack", "http://my.server.com/pl3xiconspack.zip");
        ICONS_PER_PAGE = config.getInt("icons-per-page", 9);
    }
}
