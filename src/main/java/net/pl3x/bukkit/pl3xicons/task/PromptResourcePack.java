package net.pl3x.bukkit.pl3xicons.task;

import net.pl3x.bukkit.pl3xicons.Pl3xIcons;
import net.pl3x.bukkit.pl3xicons.configuration.Config;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class PromptResourcePack extends BukkitRunnable {
    private final Player player;

    public PromptResourcePack(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        Pl3xIcons.getProtocolLibManager().setResourcePack(player, Config.ICON_PACK);
    }
}
