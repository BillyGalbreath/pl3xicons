package net.pl3x.bukkit.pl3xicons.manager;

import net.pl3x.bukkit.pl3xicons.api.Icon;
import net.pl3x.bukkit.pl3xicons.api.IconManager;
import net.pl3x.bukkit.pl3xicons.configuration.EmoticonConfig;
import net.pl3x.bukkit.pl3xicons.configuration.IconConfig;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Pl3xIconManager extends IconManager {
    private final List<Icon> icons = new ArrayList<>();
    private final LinkedHashMap<String, String> emoticons = new LinkedHashMap<>();
    private final Pattern regex = Pattern.compile("\\{(.*?)\\}");

    @Override
    public void addIcon(Icon icon) {
        if (icon == null) {
            return;
        }
        icons.add(icon);
    }

    @Override
    public List<Icon> getAll() {
        return icons;
    }

    @Override
    public void clearIcons() {
        icons.clear();
        emoticons.clear();
    }

    @Override
    public List<String> getMatching(String string) {
        List<String> list = new ArrayList<>();
        if (string == null) {
            return list;
        }
        string = string.replace("-", "_").toLowerCase();
        for (Icon icon : getAll()) {
            String code = icon.getCode();
            if (code.startsWith(string)) {
                list.add(code);
            }
        }
        return list;
    }

    @Override
    public Icon getIconByCode(String code) {
        code = code.replace("-", "_").toLowerCase();
        for (Icon icon : getAll()) {
            if (icon.getCode().equals(code)) {
                return icon;
            }
        }
        return null;
    }

    @Override
    public Icon getIconByHex(String hex) {
        hex = hex.toLowerCase();
        for (Icon icon : getAll()) {
            if (icon.getHex().equals(hex)) {
                return icon;
            }
        }
        return null;
    }

    @Override
    public Icon getIconByChar(Character character) {
        return getIconByHex(String.format("%04x", (int) character));
    }

    @Override
    public String translate(String string) {
        return translate(string, true);
    }

    @Override
    public String translate(String string, boolean parseEmoticons) {
        if (string == null) {
            return null;
        }

        if (parseEmoticons) {
            for (Map.Entry<String, String> entry : emoticons.entrySet()) {
                string = string.replace(entry.getKey(), entry.getValue());
            }
        }

        Matcher match = regex.matcher(string);
        while (match.find()) {
            String code = match.group(1);
            if (code == null) {
                continue;
            }
            Icon icon = getIconByCode(code);
            if (icon == null) {
                continue;
            }
            string = string.replace("{" + code + "}", icon.getCharacter().toString());
        }
        return string;
    }

    @Override
    public String untranslate(String string) {
        if (string == null) {
            return null;
        }
        StringBuilder newString = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            Character character = string.charAt(i);
            Icon icon = getIconByChar(character);
            if (icon == null) {
                newString.append(character);
                continue;
            }
            newString.append("{").append(icon.getCode()).append("}");
        }
        return newString.toString();
    }

    @Override
    public void reloadIconConfig() {
        clearIcons();

        FileConfiguration iconConfig = IconConfig.getConfig();
        for (String hex : iconConfig.getKeys(false)) {
            if (hex == null || hex.length() < 4 || hex.length() > 4) {
                continue;
            }
            String code = iconConfig.getString(hex);
            if (code == null || code.isEmpty() || code.equals("__UNUSED__")) {
                continue;
            }
            addIcon(new Icon(hex, code));
        }

        FileConfiguration emoticonConfig = EmoticonConfig.getConfig();
        for (String key : emoticonConfig.getKeys(false)) {
            if (key == null || key.isEmpty()) {
                continue;
            }
            String replacement = emoticonConfig.getString(key);
            if (replacement == null || replacement.isEmpty()) {
                continue;
            }
            emoticons.put(key, replacement);
        }
    }
}
