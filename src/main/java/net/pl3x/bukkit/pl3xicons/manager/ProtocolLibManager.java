package net.pl3x.bukkit.pl3xicons.manager;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.FieldAccessException;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import net.pl3x.bukkit.pl3xicons.Logger;
import net.pl3x.bukkit.pl3xicons.Pl3xIcons;
import net.pl3x.bukkit.pl3xicons.api.Icon;
import net.pl3x.bukkit.pl3xicons.api.IconManager;
import net.pl3x.bukkit.pl3xicons.api.ResourcePackStatus;
import net.pl3x.bukkit.pl3xicons.api.event.ResourcePackSetEvent;
import net.pl3x.bukkit.pl3xicons.api.event.ResourcePackStatusEvent;
import net.pl3x.bukkit.pl3xicons.configuration.Config;
import net.pl3x.bukkit.pl3xicons.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class ProtocolLibManager {
    private boolean listenerAlreadyAdded = false;

    public void registerPacketListener(Pl3xIcons plugin) {
        if (listenerAlreadyAdded) {
            return;
        }
        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Client.RESOURCE_PACK_STATUS) {
            public void onPacketReceiving(PacketEvent event) {
                if (!Config.NOTIFICATIONS) {
                    return;
                }

                PacketContainer packet = event.getPacket();
                ResourcePackStatus status = ResourcePackStatus.byName(packet.getResourcePackStatus().read(0).name());
                String hash = "";
                try {
                    hash = packet.getStrings().read(0);
                } catch (FieldAccessException ignore) {
                    // v1.10 removed hash string field "a" from packet
                }

                ResourcePackStatusEvent statusEvent = new ResourcePackStatusEvent(event.getPlayer(), status, hash);
                Bukkit.getPluginManager().callEvent(statusEvent);
            }
        });

        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Server.CHAT) {
            @SuppressWarnings("unchecked")
            public void onPacketSending(PacketEvent event) {
                try {
                    PacketContainer packet = event.getPacket();
                    String raw = packet.getChatComponents().read(0).getJson();

                    JSONObject json = (JSONObject) (new JSONParser()).parse(raw);

                    Object obj = json.get("translate");
                    if (obj == null) {
                        // regular chat
                        parseChat(json);
                    } else if (obj.toString().equals("chat.type.announcement")) {
                        // /say command
                        parseAnnouncement(json);
                    } else {
                        // we only care about regular chat and announcements
                        return;
                    }

                    packet.getChatComponents().write(0, WrappedChatComponent.fromJson(json.toJSONString()));
                } catch (NullPointerException ignore) {
                    // ignore. this happens when plugins send via chat component api
                } catch (Exception ignore) {
                    if (Config.DEBUG_MODE) {
                        Logger.debug("The following stacktrace is related to icon hover tooltips.");
                        Logger.debug("You are seeing this because something has gone wrong with parsing the JSON format and you have debugging enabled.");
                        Logger.debug("This error is safe to ignore.");
                        ignore.printStackTrace();
                    }
                }
            }
        });
        listenerAlreadyAdded = true;
    }

    public void setResourcePack(Player player, String url) {
        String hash = "Pl3xIcons";

        ResourcePackSetEvent setEvent = new ResourcePackSetEvent(player, url, hash);
        Bukkit.getPluginManager().callEvent(setEvent);

        PacketContainer packet = new PacketContainer(PacketType.Play.Server.RESOURCE_PACK_SEND);
        packet.getStrings().write(0, url).write(1, hash);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
        } catch (InvocationTargetException e) {
            Logger.error(Lang.ERROR_SENDING_RESOURCE_PACK);
            e.printStackTrace();
        }
    }

    private void parseAnnouncement(JSONObject json) {
        JSONArray with = (JSONArray) json.get("with");
        String senderName = ((JSONObject) with.get(0)).get("text").toString();

        boolean translate = false;
        IconManager iconManager = IconManager.getManager();
        //noinspection deprecation
        Player sender = Bukkit.getPlayer(senderName);
        if (sender == null || sender.hasPermission("icon.chat")) {
            translate = true;
        }

        JSONArray extras = (JSONArray) ((JSONObject) with.get(1)).get("extra");
        for (JSONObject extra : (Iterable<JSONObject>) extras) {
            String text = extra.get("text").toString();
            text = translate ? iconManager.translate(text) : iconManager.untranslate(text);
            extra.put("text", text);
        }

        parseChat((JSONObject) with.get(1));
    }

    private void parseChat(JSONObject json) {
        JSONArray extras = (JSONArray) json.get("extra");
        if (extras == null) {
            return;
        }
        for (int i = 0; i < extras.size(); i++) {
            JSONObject original = (JSONObject) extras.get(i);
            List<JSONObject> newExtras = splitExtra(original);
            int count = 0;
            for (JSONObject extra : newExtras) {
                if (count == 0) {
                    original.put("text", extra.get("text"));
                    if (extra.get("hoverEvent") != null) {
                        original.put("hoverEvent", extra.get("hoverEvent"));
                    }
                    count++;
                    continue;
                }
                i++;
                extras.add(i, extra);
            }
        }
    }

    private List<JSONObject> splitExtra(JSONObject extra) {
        IconManager iconManager = IconManager.getManager();
        boolean addHover = true;

        if (extra.get("hoverEvent") != null) {
            addHover = false; // do not overwrite existing hover event
        }

        String text = extra.get("text").toString();
        List<JSONObject> list = new ArrayList<>();
        JSONObject last = null;
        for (int c = 0; c < text.length(); c++) {
            Character character = text.charAt(c);

            JSONObject obj;
            Icon icon = iconManager.getIconByChar(character);
            if (icon == null) {
                if (last == null) {
                    obj = (JSONObject) extra.clone();
                    obj.put("text", character.toString());
                    list.add(obj);
                    last = obj;
                } else {
                    obj = last;
                    obj.put("text", obj.get("text") + character.toString());
                }
            } else {
                obj = (JSONObject) extra.clone();
                obj.put("text", character.toString());
                if (addHover) {
                    obj.put("hoverEvent", createTooltip(icon));
                }
                list.add(obj);
                last = null;
            }
        }
        return list;
    }

    private JSONObject createTooltip(Icon icon) {
        JSONObject text = new JSONObject();
        text.put("text", "{" + icon.getCode() + "}");

        JSONObject tooltip = new JSONObject();
        tooltip.put("action", "show_text");
        tooltip.put("value", text);

        return tooltip;
    }
}
