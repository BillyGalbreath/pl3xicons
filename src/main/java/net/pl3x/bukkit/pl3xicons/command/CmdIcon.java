package net.pl3x.bukkit.pl3xicons.command;

import net.pl3x.bukkit.pl3xicons.Pl3xIcons;
import net.pl3x.bukkit.pl3xicons.api.Icon;
import net.pl3x.bukkit.pl3xicons.api.IconManager;
import net.pl3x.bukkit.pl3xicons.configuration.Config;
import net.pl3x.bukkit.pl3xicons.configuration.EmoticonConfig;
import net.pl3x.bukkit.pl3xicons.configuration.IconConfig;
import net.pl3x.bukkit.pl3xicons.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdIcon implements TabExecutor {
    private final Pl3xIcons plugin;

    public CmdIcon(Pl3xIcons plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            return null;
        }
        return IconManager.getManager().getMatching(args[args.length - 1]);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.icon")) {
            Lang.send(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.ERROR_INVALID_COMMAND_STRUCTURE);
            return false; // show usage
        }

        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("command.icon.reload")) {
                Lang.send(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
                return true;
            }

            Config.reload();
            Lang.reload();
            IconConfig.reloadConfig();
            EmoticonConfig.reload();
            IconManager.getManager().reloadIconConfig();

            Lang.send(sender, Lang.RELOAD
                    .replace("{version}", plugin.getDescription().getVersion())
                    .replace("{plugin}", plugin.getName()));
            return true;
        }

        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.ERROR_PLAYER_COMMAND);
            return true;
        }

        IconManager iconManager = IconManager.getManager();

        if (args[0].equalsIgnoreCase("list")) {
            int page = 0;
            if (args.length >= 2) {
                try {
                    page = Integer.valueOf(args[1]);
                } catch (NumberFormatException e) {
                    Lang.send(sender, Lang.LIST_INVALID_PAGE_NUMBER);
                    return true;
                }
            }
            int count = 0;
            int perPage = Config.ICONS_PER_PAGE;
            List<Icon> icons = iconManager.getAll();
            int total = icons.size() / perPage;
            if (page > total) {
                Lang.send(sender, Lang.LIST_INVALID_PAGE_NUMBER);
                return true;
            }
            Lang.send(sender, Lang.LIST_PAGE_TITLE
                    .replace("{page}", Integer.toString(page))
                    .replace("{total}", Integer.toString(total)));
            for (int i = page * perPage; count < perPage; i++) {
                if (i >= icons.size()) {
                    break; // prevents ArrayIndexOutOfBoundsException
                }
                Lang.send(sender, Lang.LIST_ICON_ENTRY
                        .replace("{icon}", icons.get(i).getCharacter().toString())
                        .replace("{code}", icons.get(i).getCode()));
                count++;
            }
            return true;
        }

        Icon icon = Icon.getIcon(args[0].replace("{", "").replace("}", ""));
        if (icon == null) {
            Lang.send(sender, Lang.ERROR_NOT_VALID_ICON_CODE);
            return true;
        }

        Lang.send(sender, Lang.LIST_ICON_ENTRY
                .replace("{icon}", icon.getCharacter().toString())
                .replace("{code}", icon.getCode()));
        return true;
    }
}
