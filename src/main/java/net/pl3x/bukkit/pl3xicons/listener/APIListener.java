package net.pl3x.bukkit.pl3xicons.listener;

import net.pl3x.bukkit.pl3xicons.api.IconManager;
import net.pl3x.bukkit.pl3xicons.api.event.translate.IconTranslateEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class APIListener implements Listener {
    @SuppressWarnings("deprecation")
    @EventHandler
    public void onTranslateString(IconTranslateEvent event) {
        event.setTranslatedString(IconManager.getManager().translate(event.getString()));
        event.setUntranslatedString(IconManager.getManager().untranslate(event.getString()));
    }
}
