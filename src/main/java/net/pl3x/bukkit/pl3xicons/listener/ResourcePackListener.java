package net.pl3x.bukkit.pl3xicons.listener;

import net.pl3x.bukkit.pl3xicons.Logger;
import net.pl3x.bukkit.pl3xicons.Pl3xIcons;
import net.pl3x.bukkit.pl3xicons.api.event.ResourcePackStatusEvent;
import net.pl3x.bukkit.pl3xicons.configuration.Config;
import net.pl3x.bukkit.pl3xicons.configuration.Lang;
import net.pl3x.bukkit.pl3xicons.task.PromptResourcePack;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ResourcePackListener implements Listener {
    private final Pl3xIcons plugin;

    public ResourcePackListener(Pl3xIcons plugin) {
        this.plugin = plugin;
        Pl3xIcons.getProtocolLibManager().registerPacketListener(plugin);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onResourcePackStatus(ResourcePackStatusEvent event) {
        Player player = event.getPlayer();
        Logger.debug("&2[&7ResourcePackStatusEvent&2] &ePlayer: " + player.getName() + " Status: " + event.getStatus() + " Hash: " + event.getHash());

        switch (event.getStatus()) {
            case ACCEPTED:
                if (Config.NOTIFICATIONS) {
                    Lang.send(player, Lang.RESOURCE_PACK_ACCEPTED);
                }
                return;
            case SUCCESSFULLY_LOADED:
                if (Config.NOTIFICATIONS) {
                    Lang.send(player, Lang.RESOURCE_PACK_LOADED);
                }
                return;
            case DECLINED:
                if (Config.NOTIFICATIONS) {
                    Lang.send(player, Lang.RESOURCE_PACK_DECLINED);
                }
            case FAILED_DOWNLOAD:
                if (Config.NOTIFICATIONS) {
                    Lang.send(player, Lang.RESOURCE_PACK_DOWNLOAD_FAILED);
                }
        }

        if (Config.FORCE) {
            Bukkit.getScheduler().runTaskLater(plugin,
                    () -> player.kickPlayer(Lang.FORCE_KICK_MESSAGE), 5);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!Config.SEND_RESOURCE_PACK) {
            return; // config disabled sending resource pack
        }
        new PromptResourcePack(event.getPlayer()).runTaskLaterAsynchronously(plugin, 20);
    }
}
