package net.pl3x.bukkit.pl3xicons.listener;

import net.pl3x.bukkit.pl3xicons.Logger;
import net.pl3x.bukkit.pl3xicons.api.IconManager;
import net.pl3x.bukkit.pl3xicons.api.event.translate.AnvilResultEvent;
import net.pl3x.bukkit.pl3xicons.api.event.translate.ChatEvent;
import net.pl3x.bukkit.pl3xicons.api.event.translate.EditBookEvent;
import net.pl3x.bukkit.pl3xicons.api.event.translate.SignEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BukkitListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        ChatEvent chatEvent = new ChatEvent(player, event.getFormat(), event.getMessage(), event.getRecipients());
        Bukkit.getPluginManager().callEvent(chatEvent);
        if (chatEvent.isCancelled()) {
            Logger.debug("[ChatEvent] A plugin has cancelled the event.");
            return;
        }
        IconManager iconManager = IconManager.getManager();
        if (player.hasPermission("icon.chat")) {
            Logger.debug("[ChatEvent] Icons allowed. Scanning text for replacement.");
            event.setMessage(iconManager.translate(chatEvent.getMessage()));
        } else {
            Logger.debug("[ChatEvent] Icons NOT allowed. Scanning text for removal.");
            event.setMessage(iconManager.untranslate(chatEvent.getMessage()));
        }
        if (player.hasPermission("icon.nick")) {
            Logger.debug("[ChatEvent] Icons allowed in nick. Scanning text for replacement.");
            player.setDisplayName(iconManager.translate(player.getDisplayName()));
        } else {
            Logger.debug("[ChatEvent] Icons NOT allowed in nick. Scanning text for removal.");
            player.setDisplayName(iconManager.untranslate(player.getDisplayName()));
        }

        event.setFormat(iconManager.translate(chatEvent.getFormat()));
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onSignChange(SignChangeEvent event) {
        Player player = event.getPlayer();
        SignEvent signEvent = new SignEvent(player, event.getBlock(), event.getLines());
        Bukkit.getPluginManager().callEvent(signEvent);
        if (signEvent.isCancelled()) {
            Logger.debug("[SignEvent] A plugin has cancelled the event.");
            return;
        }
        IconManager iconManager = IconManager.getManager();
        if (player.hasPermission("icon.sign")) {
            Logger.debug("[SignEvent] Icons allowed. Scanning text for replacement.");
            for (int i = 0; i < 4; i++) {
                event.setLine(i, iconManager.translate(signEvent.getLine(i)));
            }
        } else {
            Logger.debug("[SignEvent] Icons NOT allowed. Scanning text for removal.");
            for (int i = 0; i < 4; i++) {
                event.setLine(i, iconManager.untranslate(signEvent.getLine(i)));
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent event) {
        HumanEntity ent = event.getWhoClicked();
        if (!(ent instanceof Player)) {
            return;
        }
        Player player = (Player) ent;
        Inventory inv = event.getInventory();
        if (!(inv instanceof AnvilInventory)) {
            return;
        }
        InventoryView view = event.getView();
        int rawSlot = event.getRawSlot();
        if (rawSlot != view.convertSlot(rawSlot)) {
            return;
        }
        if (rawSlot != 2) {
            return;
        }
        ItemStack item = event.getCurrentItem();
        if (item == null) {
            return;
        }
        ItemMeta meta = item.getItemMeta();
        if (meta == null) {
            return;
        }
        if (!meta.hasDisplayName()) {
            return;
        }
        Set<Material> transparent = new HashSet<>();
        transparent.add(Material.AIR);
        AnvilResultEvent anvilResultEvent = new AnvilResultEvent(player, player.getTargetBlock(transparent, 7), item);
        Bukkit.getPluginManager().callEvent(anvilResultEvent);
        if (anvilResultEvent.isCancelled()) {
            Logger.debug("[AnvilResultEvent] A plugin has cancelled the event.");
            return;
        }
        IconManager iconManager = IconManager.getManager();
        if (item.getType().equals(Material.NAME_TAG)) {
            if (player.hasPermission("icon.nametag")) {
                Logger.debug("[AnvilResultEvent NameTag] Icons allowed. Scanning text for replacement.");
                meta.setDisplayName(iconManager.translate(meta.getDisplayName()));
            } else {
                Logger.debug("[AnvilResultEvent NameTag] Icons NOT allowed. Scanning text for removal.");
                meta.setDisplayName(iconManager.untranslate(meta.getDisplayName()));
            }
        } else {
            if (player.hasPermission("icon.anvil")) {
                Logger.debug("[AnvilResultEvent] Icons allowed. Scanning text for replacement.");
                meta.setDisplayName(iconManager.translate(meta.getDisplayName()));
            } else {
                Logger.debug("[AnvilResultEvent] Icons NOT allowed. Scanning text for removal.");
                meta.setDisplayName(iconManager.untranslate(meta.getDisplayName()));
            }
        }
        item.setItemMeta(meta);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEditBook(PlayerEditBookEvent event) {
        Player player = event.getPlayer();

        EditBookEvent editBookEvent = new EditBookEvent(player, event.getPreviousBookMeta(), event.getNewBookMeta(), event.isSigning());
        Bukkit.getPluginManager().callEvent(editBookEvent);
        if (editBookEvent.isCancelled()) {
            Logger.debug("[EditBookEvent] A plugin has cancelled the event.");
            return;
        }

        BookMeta meta = editBookEvent.getNewBookMeta();
        String title = meta.getTitle();
        String newTitle;
        List<String> pages = meta.getPages();
        List<String> newPages = new ArrayList<>();

        IconManager iconManager = IconManager.getManager();
        if (player.hasPermission("icon.book")) {
            Logger.debug("[EditBookEvent] Icons allowed. Scanning text for replacement.");
            newTitle = iconManager.translate(title);
            newPages.addAll(pages.stream().map(iconManager::translate).collect(Collectors.toList()));
        } else {
            Logger.debug("[EditBookEvent] Icons NOT allowed. Scanning text for removal.");
            newTitle = iconManager.untranslate(title);
            newPages.addAll(pages.stream().map(iconManager::untranslate).collect(Collectors.toList()));
        }

        meta.setTitle(newTitle);
        meta.setPages(newPages);

        event.setNewBookMeta(meta);
        event.setSigning(editBookEvent.isSigning());
    }
}
