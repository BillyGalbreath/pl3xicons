package net.pl3x.app.pl3xicons;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

class ZipFile {
    private final java.util.zip.ZipFile zipFile;

    ZipFile(String path, boolean create) throws IOException {
        File file = new File(path);
        if (create && !file.exists()) {
            FileOutputStream fos = new FileOutputStream(file);
            //noinspection OctalInteger
            fos.write(new byte[]{80, 75, 05, 06, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00}, 0, 22);
            fos.flush();
            fos.close();
        }
        zipFile = new java.util.zip.ZipFile(file);
    }

    byte[] getEntry(String path) {
        try {
            ZipEntry entry = zipFile.getEntry(path);
            BufferedInputStream in = new BufferedInputStream(zipFile.getInputStream(entry));

            byte[] bytes = new byte[(int) entry.getSize()];
            byte[] buffer = new byte[2048];
            int total = 0;

            int size;
            while ((size = in.read(buffer, 0, 2048)) >= 0) {
                System.arraycopy(buffer, 0, bytes, total, size);
                total += size;
            }

            in.close(); //close BufferedInputStream

            return bytes;
        } catch (IOException e) {
            System.err.println("No zip entry found at: " + path);
            return null;
        }
    }

    void saveEntry(String path, ZipOutputStream zout) {
        try {
            File file = new File(ResourcePack.tempDir, path);
            FileInputStream in = new FileInputStream(file);
            ZipEntry entry = new ZipEntry(path);
            zout.putNextEntry(entry);

            byte[] bytes = new byte[2048];
            int size;
            while ((size = in.read(bytes)) >= 0) {
                zout.write(bytes, 0, size);
            }

            zout.closeEntry();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void addDirToArchive(ZipOutputStream zos, File srcFile) throws IOException {
        //noinspection ConstantConditions
        for (File file : srcFile.listFiles()) {
            if (file.isDirectory()) {
                addDirToArchive(zos, file);
                continue;
            }

            String path = file.getCanonicalPath()
                    .substring(ResourcePack.tempDir.getCanonicalPath().length() + 1,
                            file.getCanonicalPath().length())
                    .replace(File.separator, "/");

            FileInputStream in = new FileInputStream(file);
            zos.putNextEntry(new ZipEntry(path));

            byte[] buffer = new byte[2048];
            int size;
            while ((size = in.read(buffer)) > 0) {
                zos.write(buffer, 0, size);
            }

            zos.closeEntry();
            in.close();
        }
    }
}
