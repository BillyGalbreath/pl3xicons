package net.pl3x.app.pl3xicons;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import net.pl3x.app.pl3xicons.gui.Icon;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

public class ResourcePack {
    private final Main main;
    private final Map<String, Icon> icons = new HashMap<>();
    private final Map<String, Boolean> pages = new TreeMap<>();
    private String fileLocation;

    final static File tempDir = new File(System.getProperty("user.home"), ".pl3xicons");

    ResourcePack(Main main) {
        this.main = main;
    }

    public File getFile() {
        if (fileLocation == null) {
            return null;
        }
        return new File(fileLocation);
    }

    public Icon getIcon(String hex) {
        return icons.get(hex);
    }

    public void setIcon(String hex, Icon icon) {
        icons.put(hex, icon);
        pages.put(hex.substring(0, 2), true);
    }

    public String getFirstActivePage() {
        for (Map.Entry<String, Boolean> entry : pages.entrySet()) {
            if (entry.getValue()) {
                return entry.getKey();
            }
        }
        return "00";
    }

    Map<String, Boolean> getPages() {
        return pages;
    }

    public boolean openResourcePack(String file) {
        icons.clear();
        pages.clear();
        fileLocation = file;

        // open zip file
        ZipFile zipFile;
        try {
            zipFile = new ZipFile(file, false);
        } catch (IOException e) {
            System.err.println("Invalid IconPack Format: Unable to load zip file at location: " + file);
            JOptionPane.showMessageDialog(null, "Invalid IconPack Format:\nUnable to load zip file at location:\n" + file);
            return false;
        }

        // Read icons.yml: Map<hex, code>
        Map<String, String> codes = new HashMap<>();
        try {
            InputStream in = new ByteArrayInputStream(zipFile.getEntry("icons.yml"));
            Yaml yaml = new Yaml();
            Object yamlObj = yaml.load(in);
            in.close();

            //noinspection unchecked
            codes.putAll((Map<String, String>) yamlObj);

            System.out.println("Loaded " + codes.size() + " entries from icons.yml");
        } catch (Exception e) {
            System.err.println("Invalid IconPack Format:  Missing or invalid icons.yml");
            JOptionPane.showMessageDialog(null, "Invalid IconPack Format:\nMissing or invalid icons.yml");
            return false;
        }

        // Load font pages
        int counter = 0;
        double total = 65536D;
        boolean hasActivePage = false;
        for (int page = 0; page < 256; page++) {
            // get the hex code for this page
            String hexPage = getHex(Integer.toHexString(page));

            BufferedImage sprite;
            try {
                InputStream in = new ByteArrayInputStream(zipFile.getEntry("assets/minecraft/textures/font/unicode_page_" + hexPage + ".png"));
                sprite = ImageIO.read(in);
                in.close();
                pages.put(hexPage, true);
                hasActivePage = true;
            } catch (Exception e1) {
                try {
                    //noinspection ConstantConditions
                    sprite = ImageIO.read(getClass().getClassLoader().getResource("images/font/unicode_page_" + hexPage + ".png"));
                    pages.put(hexPage, false);
                } catch (Exception e2) {
                    // something went wrong or no page found
                    total -= 256D;
                    main.getStatusBar().setStatus("Loading icon pack. Please wait... " + (int) (((double) counter / total) * 100D) + "%");
                    continue;
                }
            }

            // split image up
            int width = sprite.getWidth() / 16;
            int height = sprite.getHeight() / 16;
            for (int y = 0; y < 16; y++) {
                for (int x = 0; x < 16; x++) {
                    counter++;
                    main.getStatusBar().setStatus("Loading icon pack. Please wait... " + (int) (((double) counter / total) * 100D) + "%");

                    // get the hex code for this character
                    String hex = hexPage + getHex(Integer.toHexString(y) + Integer.toHexString(x));

                    // splice image and store icon
                    icons.put(hex, new Icon(main, hex, codes.get(hex), sprite.getSubimage(x * width, y * height, width, height)));
                }
            }
        }

        if (!hasActivePage) {
            System.err.println("Invalid IconPack Format:  Missing font pages");
            JOptionPane.showMessageDialog(null, "Invalid IconPack Format:\nMissing font pages");
            return false;
        }

        return true;
    }

    public void saveResourcePack(String file) {
        // open/create zip file
        ZipFile zipFile;
        try {
            zipFile = new ZipFile(file, true);
        } catch (IOException e) {
            System.err.println("Save Error: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "SaveError:\n" + e.getMessage());
            return;
        }

        // Save icons.yml
        try {
            // construct yaml data
            Map<String, Object> codes = new HashMap<>();
            icons.values().stream()
                    .filter(icon -> icon.getRawCode() != null)
                    .forEachOrdered(icon -> codes.put(icon.getHex(), icon.getRawCode()));

            // write yaml file
            DumperOptions options = new DumperOptions();
            options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            options.setPrettyFlow(true);
            Yaml yaml = new Yaml(options);
            FileWriter writer = new FileWriter(createRootFile("icons.yml"));
            yaml.dump(codes, writer);
            writer.close();

            //noinspection unchecked
            System.out.println("Saved " + codes.size() + " entries to icons.yml");
        } catch (Exception e) {
            System.err.println("Save Error:  Problem saving icons.yml");
            JOptionPane.showMessageDialog(null, "Save Error:\nproblem saving icons.yml");
            return;
        }

        try {
            double total = 0;
            for (boolean page : pages.values()) {
                if (page) {
                    total += 256D;
                }
            }

            int counter = 0;
            // Save font pages
            for (int page = 0; page < 256; page++) {
                // get the hex code for this page
                String hexPage = getHex(Integer.toHexString(page));

                // check if this page has an active icon
                if (pages.get(hexPage) == null || !pages.get(hexPage)) {
                    continue;
                }

                // setup combined image
                BufferedImage sampleImage = icons.get(hexPage + "00").getImage();
                int w = sampleImage.getWidth();
                int h = sampleImage.getHeight();

                // loop page slices
                BufferedImage finalImg = new BufferedImage(w * 16, h * 16, BufferedImage.TYPE_INT_ARGB);
                for (int y = 0; y < 16; y++) {
                    for (int x = 0; x < 16; x++) {
                        counter++;
                        main.getStatusBar().setStatus("Saving icon pack. Please wait... " + (int) (((double) counter / total) * 100D) + "%");

                        // get the hex code for this character
                        String hex = hexPage + getHex(Integer.toHexString(y) + Integer.toHexString(x));

                        // get icon
                        BufferedImage iconImage = icons.get(hex).getImage();

                        // add to final image
                        finalImg.createGraphics().drawImage(iconImage, iconImage.getWidth() * x, iconImage.getHeight() * y, null);
                    }
                }

                ImageIO.write(finalImg, "png", createImageFile("unicode_page_" + hexPage + ".png"));
            }
        } catch (IOException e) {
            System.err.println("Save Error: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "SaveError:\n" + e.getMessage());
            return;
        }

        // save pack files
        addFile("pack.mcmeta");
        addFile("spyc.php");
        addFile("index.php");


        // pack all files into zip
        try {
            FileOutputStream fout = new FileOutputStream(file);
            ZipOutputStream zout = new ZipOutputStream(fout);

            zout.putNextEntry(new ZipEntry("assets/"));

            zipFile.saveEntry("icons.yml", zout);
            zipFile.saveEntry("pack.mcmeta", zout);
            zipFile.saveEntry("spyc.php", zout);
            zipFile.saveEntry("index.php", zout);
            zipFile.addDirToArchive(zout, new File(tempDir, "assets"));

            zout.close();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Save Error: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "SaveError:\n" + e.getMessage());
            return;
        }

        // empty temp directory
        deleteDir(tempDir);
    }

    private String getHex(String hex) {
        if (hex.length() == 1) {
            hex = "0" + hex;
        }
        return hex;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private File createRootFile(String name) throws IOException {
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        File file = new File(tempDir, name);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        return file;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private File createImageFile(String name) throws IOException {
        File dir = new File(new File(new File(new File(tempDir, "assets"), "minecraft"), "textures"), "font");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir, name);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        return file;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }

    private void addFile(String file) {
        try {
            InputStreamReader in = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(file));
            BufferedReader input = new BufferedReader(in);
            FileWriter out = new FileWriter(createRootFile(file));
            BufferedWriter output = new BufferedWriter(out);

            char[] buffer = new char[2048];
            int n;
            while ((n = input.read(buffer)) >= 0) {
                output.write(buffer, 0, n);
            }

            input.close();
            output.close();
        } catch (IOException e) {
            System.err.println("Save Error: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "SaveError:\n" + e.getMessage());
        }
    }
}
