package net.pl3x.app.pl3xicons;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.util.ArrayList;

class Logo {
    public static ArrayList<Image> getIcons() {
        ArrayList<Image> icons = new ArrayList<>();

        icons.add(loadIconQuietly(16));
        icons.add(loadIconQuietly(32));
        icons.add(loadIconQuietly(48));
        icons.add(loadIconQuietly(256));

        return icons;
    }

    private static Image loadIconQuietly(int size) {
        try {
            return ImageIO.read(Logo.class.getResource("/images/pl3x_" + size + ".png"));
        } catch (Exception e) {
            System.err.println("Error: failed to load " + size + "px icon");
        }
        return null;
    }
}
