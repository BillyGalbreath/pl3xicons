package net.pl3x.app.pl3xicons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

public class FlipPage implements ActionListener {
    private final Main main;
    private final int direction;

    public FlipPage(Main main, int direction) {
        this.main = main;
        this.direction = direction;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        TreeMap<String, Boolean> tree = (TreeMap<String, Boolean>) main.getResourcePack().getPages();
        int page = Integer.parseInt(main.getPage(), 16);
        int last = Integer.parseInt(tree.lastKey(), 16);
        int first = Integer.parseInt(tree.firstKey(), 16);
        String newPage = null;
        int failsafe = 0; // prevents accidental infinite loops
        while (main.getResourcePack().getIcon(newPage + "00") == null && failsafe < 300) {
            switch (direction) {
                case 1:
                    page = page >= last ? first : page + 1;
                    break;
                case -1:
                    page = page <= first ? last : page - 1;
                    break;
            }
            newPage = Integer.toHexString(page);
            if (newPage.length() == 1) {
                newPage = "0" + newPage;
            }
            failsafe++;
        }
        main.setPage(newPage);
    }
}
