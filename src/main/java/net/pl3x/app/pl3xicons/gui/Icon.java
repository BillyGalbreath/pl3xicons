package net.pl3x.app.pl3xicons.gui;

import net.pl3x.app.pl3xicons.Main;
import net.pl3x.app.pl3xicons.gui.editicon.EditDialog;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class Icon extends JLabel implements MouseListener {
    private final Main main;
    private final String hex;
    private String code;
    private final char character;
    private BufferedImage image;

    public Icon(Main main, String hex, String code, BufferedImage image) {
        super();

        this.main = main;
        this.hex = hex;
        this.code = code;
        this.image = image;

        this.character = (char) Integer.parseInt(hex, 16);

        setOpaque(true);
        setHorizontalAlignment(JLabel.CENTER);
        setIcon(new ImageIcon(getScaledImage()));
        setBackground(null);
        setToolTip();
        setVisible(true);

        addMouseListener(this);
    }

    private void setToolTip() {
        boolean unused = getCode() == null;
        setToolTipText("<html><div style='margin:0 -3 0 -3;padding:0 3 0 3;background:#" +
                (unused ? "e2727b" : "ffffca") +
                "'><b>Hex:</b>: 0x" +
                getHex() +
                "<br><b>Code</b>: " +
                (unused ? "<em style='color:#990000'><b>UNUSED</b></em>" : getCode()) +
                "<br><b>Char</b>: " +
                getCharacter() +
                "</div></html>");
    }

    public String getHex() {
        return hex;
    }

    private String getCode() {
        if (code == null || code.isEmpty() || code.equals("__UNUSED__")) {
            return null;
        }
        return "{" + code.toLowerCase().replace("_", "-") + "}";
    }

    public void setCode(String code) {
        this.code = code.replace("{", "").replace("}", "").replace("-", "_").toUpperCase();
        setToolTip();
    }

    public String getRawCode() {
        if (code == null || code.isEmpty() || code.equals("__UNUSED__")) {
            return null;
        }
        return code.toUpperCase().replace("-", "_");
    }

    public char getCharacter() {
        return character;
    }

    public BufferedImage getImage() {
        return image;
    }

    private BufferedImage getScaledImage() {
        BufferedImage scaled = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
        Graphics g = scaled.createGraphics();
        g.drawImage(getImage(), 0, 0, 32, 32, null);
        g.dispose();
        return scaled;
    }

    public void setImage(javax.swing.Icon icon) {
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics g = image.createGraphics();
        icon.paintIcon(null, g, 0, 0);
        g.dispose();

        this.image = image;
        setIcon(new ImageIcon(getScaledImage()));
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        new EditDialog(main, this);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        setBackground(Color.BLUE);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setBackground(null);
    }
}
