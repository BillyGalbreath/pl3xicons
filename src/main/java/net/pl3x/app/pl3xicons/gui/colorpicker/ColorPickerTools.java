package net.pl3x.app.pl3xicons.gui.colorpicker;

import net.pl3x.app.pl3xicons.gui.Button;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import java.awt.Color;

public class ColorPickerTools extends JPanel {
    private final JPanel colorPreview;
    private final Button btnPencil;
    private final Button btnEraser;
    private final Button btnDropper;
    private ToolType toolType;

    ColorPickerTools() {
        setLayout(null);
        setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
        setBounds(230, 127, 387, 60);

        ColorPreviewHolder colorPreviewHolder = new ColorPreviewHolder();
        colorPreviewHolder.setLayout(null);
        colorPreviewHolder.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        colorPreviewHolder.setBounds(5, 5, 50, 50);
        add(colorPreviewHolder);

        colorPreview = new JPanel();
        colorPreview.setLayout(null);
        colorPreview.setBounds(2, 2, 46, 46);
        colorPreviewHolder.add(colorPreview);

        btnPencil = new Button("images/tool_pencil.png", ToolType.PENCIL);
        btnPencil.setBounds(148, 16, 25, 25);
        btnPencil.setState(true);

        btnEraser = new Button("images/tool_eraser.png", ToolType.ERASER);
        btnEraser.setBounds(203, 16, 25, 25);
        btnEraser.setState(false);

        btnDropper = new Button("images/tool_dropper.png", ToolType.DROPPER);
        btnDropper.setBounds(258, 16, 25, 25);
        btnDropper.setState(false);

        add(btnPencil);
        add(btnEraser);
        add(btnDropper);

        setTool(ToolType.PENCIL);
    }

    void setColor(Color color) {
        colorPreview.setBackground(color);
    }

    public void setTool(ToolType toolType) {
        this.toolType = toolType;

        btnPencil.setState(false);
        btnEraser.setState(false);
        btnDropper.setState(false);

        switch (toolType) {
            case DROPPER:
                btnDropper.setState(true);
                break;
            case ERASER:
                btnEraser.setState(true);
                break;
            case PENCIL:
            default:
                btnPencil.setState(true);
        }
    }

    public ToolType getToolType() {
        return toolType;
    }

    public enum ToolType {
        PENCIL, ERASER, DROPPER
    }
}
