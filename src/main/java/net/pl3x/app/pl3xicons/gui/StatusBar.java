package net.pl3x.app.pl3xicons.gui;

import javax.swing.JLabel;
import javax.swing.border.BevelBorder;

public class StatusBar extends JLabel {
    public StatusBar() {
        setLayout(null);
        setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        setBounds(0, 565, 525, 22);

        setText("Ready.");

        setVisible(true);
    }

    public void setStatus(String status) {
        setText(status);
        paintImmediately(getVisibleRect());
    }
}
