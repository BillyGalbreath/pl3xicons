package net.pl3x.app.pl3xicons.gui;

import net.pl3x.app.pl3xicons.FlipPage;
import net.pl3x.app.pl3xicons.Main;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class PageControls extends JPanel {
    private final JButton previousButton;
    private final JButton nextButton;
    private final Main main;
    private final JTextField pageText;

    public PageControls(Main main) {
        this.main = main;

        setLayout(null);
        setBounds(0, 525, 525, 40);

        previousButton = new JButton("Prev");
        previousButton.setFont(new Font(previousButton.getFont().getName(), Font.BOLD, 14));
        previousButton.setBounds(102, 5, 75, 30);
        previousButton.setVisible(true);
        previousButton.setEnabled(false);
        previousButton.addActionListener(new FlipPage(main, -1));

        nextButton = new JButton("Next");
        nextButton.setFont(new Font(nextButton.getFont().getName(), Font.BOLD, 14));
        nextButton.setBounds(352, 5, 75, 30);
        nextButton.setVisible(true);
        nextButton.setEnabled(false);
        nextButton.addActionListener(new FlipPage(main, 1));

        pageText = new JTextField();
        pageText.setFont(new Font(pageText.getFont().getName(), Font.BOLD, 16));
        pageText.setBounds(220, 5, 90, 30);
        pageText.setHorizontalAlignment(SwingConstants.CENTER);
        pageText.setEnabled(false);
        pageText.setEditable(false);
        pageText.setVisible(true);
        pageText.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                checkPage();
            }
        });
        pageText.addActionListener(e -> checkPage());

        add(previousButton);
        add(nextButton);
        add(pageText);

        requestFocus();
    }

    private void checkPage() {
        String text = pageText.getText();
        if (text.length() == 1) {
            text = "0" + text;
        }
        int value = -1;
        try {
            value = Integer.parseInt(text, 16);
        } catch (NumberFormatException ignore) {
        }
        if (value < 0 || value > 255) {
            JOptionPane.showMessageDialog(null, "Error: Please enter hex value between 00 and FF", "Error Massage",
                    JOptionPane.ERROR_MESSAGE);
            pageText.setText(main.getPage());
            return;
        }
        if (main.getResourcePack().getIcon(text + "00") == null) {
            JOptionPane.showMessageDialog(null, "Error: That page does not exist!", "Error Massage",
                    JOptionPane.ERROR_MESSAGE);
            pageText.setText(main.getPage());
            return;
        }
        main.setPage(text);
    }

    public void setPage(String page) {
        pageText.setText(page);
        pageText.setEnabled(true);
        pageText.setEditable(true);
        previousButton.setEnabled(true);
        nextButton.setEnabled(true);
        requestFocus();
    }
}
