package net.pl3x.app.pl3xicons.gui.editicon;

import net.pl3x.app.pl3xicons.Main;
import net.pl3x.app.pl3xicons.gui.Icon;
import net.pl3x.app.pl3xicons.gui.colorpicker.DrawingToolsPanel;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class EditDialog extends JDialog {
    private final EditImage editImage;
    private final JLabel lblIconPreview;
    private final JTextField txtCode;
    private final DrawingToolsPanel drawingToolsPanel;
    private final JButton btnAdvanced;

    private boolean advancedMode = true;

    public EditDialog(Main main, Icon icon) {
        super(null, "Edit Icon", null);

        // setup dialog
        setSize(652, 307);
        setModal(true);
        setTitle("Edit Icon");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        // center dialog

        Dimension guiSize = main.getSize();
        Dimension dialogSize = getSize();
        Point center = main.getLocation();
        center.x = center.x + guiSize.width / 2;
        center.y = center.y + guiSize.height / 2;
        setLocation(center.x - dialogSize.width / 2, center.y - dialogSize.height / 2);


        // setup image editor panel
        editImage = new EditImage(this, icon);
        editImage.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        editImage.setBounds(5, 5, 260, 260);

        lblIconPreview = new JLabel("", new ImageIcon(icon.getImage()), JLabel.CENTER);
        txtCode = new JTextField();

        // setup buttons
        btnAdvanced = new JButton();
        //noinspection ConstantConditions
        btnAdvanced.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/arrow-down.png")));
        btnAdvanced.setBounds(269, 239, 50, 25);
        btnAdvanced.addActionListener(e -> toggleAdvancedMode());

        JButton btnImport = new JButton("Import");
        btnImport.setBounds(333, 239, 90, 25);
        btnImport.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(new FileNameExtensionFilter("Images", "png", "jpg", "jpeg", "gif", "bmp"));
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setCurrentDirectory(main.getLastImageLocation());
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                main.setLastImageLocation(selectedFile.getParent());

                try {
                    BufferedImage image = ImageIO.read(selectedFile);

                    BufferedImage scaled = new BufferedImage(editImage.getIconWidth(), editImage.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                    Graphics g = scaled.createGraphics();
                    g.drawImage(image, 0, 0, editImage.getIconWidth(), editImage.getIconHeight(), null);
                    g.dispose();

                    editImage.setImage(scaled);
                    setPreviewIcon(scaled);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        JButton btnSave = new JButton("Save");
        btnSave.setBounds(437, 239, 90, 25);
        btnSave.addActionListener(e -> {
            icon.setImage(lblIconPreview.getIcon());
            icon.setCode(txtCode.getText().trim());

            main.getResourcePack().setIcon(icon.getHex(), icon);
            main.setPage(main.getPage());

            setVisible(false);
            dispatchEvent(new WindowEvent(EditDialog.this, WindowEvent.WINDOW_CLOSING));
        });

        JButton btnCancel = new JButton("Cancel");
        btnCancel.setBounds(540, 239, 90, 25);
        btnCancel.addActionListener(e -> {
            setVisible(false);
            dispatchEvent(new WindowEvent(EditDialog.this, WindowEvent.WINDOW_CLOSING));
        });

        // setup data panel
        JPanel panelData = new JPanel();
        panelData.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        panelData.setBounds(269, 5, 362, 230);
        panelData.setLayout(null);

        JLabel lblHex = new JLabel("Hex Code:");
        lblHex.setBounds(12, 12, 80, 15);

        JLabel lblChar = new JLabel("Character:");
        lblChar.setBounds(12, 41, 80, 15);

        JLabel lblDimension = new JLabel("Dimension:");
        lblDimension.setBounds(12, 68, 80, 15);

        JLabel lblCode = new JLabel("Tag Code:");
        lblCode.setBounds(12, 97, 80, 15);

        JTextField txtHex = new JTextField();
        txtHex.setHorizontalAlignment(SwingConstants.CENTER);
        txtHex.setEditable(false);
        txtHex.setText("0x" + icon.getHex());
        txtHex.setBounds(106, 12, 245, 19);
        txtHex.setColumns(10);

        JTextField txtChar = new JTextField();
        txtChar.setHorizontalAlignment(SwingConstants.CENTER);
        txtChar.setEditable(false);
        txtChar.setText(Character.toString(icon.getCharacter()));
        txtChar.setBounds(106, 41, 245, 19);
        txtChar.setColumns(10);

        JTextField txtDimension = new JTextField();
        txtDimension.setHorizontalAlignment(SwingConstants.CENTER);
        txtDimension.setEditable(false);
        txtDimension.setText(icon.getImage().getWidth() + " x " + icon.getImage().getHeight());
        txtDimension.setBounds(106, 68, 245, 19);
        txtDimension.setColumns(10);

        txtCode.setHorizontalAlignment(SwingConstants.CENTER);
        txtCode.setText(icon.getRawCode());
        txtCode.setBounds(106, 95, 245, 19);
        txtCode.setColumns(1);

        JPanel iconPreviewPanel = new JPanel();
        iconPreviewPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        iconPreviewPanel.setBounds(147, 135, 64, 64);
        iconPreviewPanel.setLayout(null);

        lblIconPreview.setBounds(16, 16, 32, 32);
        iconPreviewPanel.add(lblIconPreview);

        panelData.add(lblHex);
        panelData.add(lblChar);
        panelData.add(lblDimension);
        panelData.add(lblCode);
        panelData.add(txtHex);
        panelData.add(txtChar);
        panelData.add(txtDimension);
        panelData.add(txtCode);
        panelData.add(iconPreviewPanel);

        // setup advanced panel
        drawingToolsPanel = new DrawingToolsPanel();
        drawingToolsPanel.getColorPicker().setLocation(1, 1);
        drawingToolsPanel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
        drawingToolsPanel.setBounds(5, 269, 626, 195);

        // add all the panels
        getContentPane().setLayout(null);
        getContentPane().add(editImage);
        getContentPane().add(panelData);
        getContentPane().add(btnImport);
        getContentPane().add(btnAdvanced);
        getContentPane().add(btnSave);
        getContentPane().add(btnCancel);
        getContentPane().add(drawingToolsPanel);

        // set correct sizes
        toggleAdvancedMode();

        // show the dialog contents
        setVisible(true);
    }

    private void toggleAdvancedMode() {
        advancedMode = !isAdvancedMode();

        drawingToolsPanel.setVisible(isAdvancedMode());
        //noinspection ConstantConditions
        btnAdvanced.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/arrow-" + (isAdvancedMode() ? "up" : "down") + ".png")));

        setSize(652, isAdvancedMode() ? 506 : 307);
    }

    boolean isAdvancedMode() {
        return advancedMode;
    }

    DrawingToolsPanel getDrawingTools() {
        return drawingToolsPanel;
    }

    void setPreviewIcon(BufferedImage image) {
        lblIconPreview.setIcon(new ImageIcon(image));
    }
}
