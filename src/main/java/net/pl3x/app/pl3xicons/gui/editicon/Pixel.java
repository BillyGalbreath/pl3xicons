package net.pl3x.app.pl3xicons.gui.editicon;

import javax.swing.JPanel;
import java.awt.Color;

class Pixel extends JPanel {
    private final EditImage editor;
    private Color color;
    private final int x, y;

    Pixel(EditImage editor, Color color, int x, int y) {
        this.editor = editor;
        this.color = color;
        this.x = x;
        this.y = y;

        setLayout(null);
        setBackground(color);
        setOpaque(true);
        setVisible(true);
    }

    Color getColor() {
        return color;
    }

    void setColor(Color color) {
        this.color = color;
        setBackground(color);
        editor.updateImage(color, x, y);
    }

    void invertColor() {
        setBackground(new Color(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue()));
    }
}
