package net.pl3x.app.pl3xicons.gui;

import net.pl3x.app.pl3xicons.gui.colorpicker.ColorPickerTools;
import net.pl3x.app.pl3xicons.gui.colorpicker.ColorPickerTools.ToolType;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Button extends JPanel implements MouseListener {
    private boolean state;
    private BufferedImage icon;
    private final ToolType toolType;

    public Button(String iconName, ToolType toolType) {
        this.toolType = toolType;

        try {
            //noinspection ConstantConditions
            this.icon = ImageIO.read(getClass().getClassLoader().getResource(iconName));
        } catch (IOException ignore) {
        }

        setLayout(null);
        setSize(new Dimension(25, 25));

        setState(state);

        addMouseListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (icon != null) {
            g.drawImage(icon, 4, 4, null);
        }
    }

    public void setState(boolean state) {
        this.state = state;
        setBorder(new BevelBorder(this.state ? BevelBorder.LOWERED : BevelBorder.RAISED, null, null, null, null));
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        ((ColorPickerTools) getParent()).setTool(toolType);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        ((ColorPickerTools) getParent()).setTool(toolType);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        ((ColorPickerTools) getParent()).setTool(toolType);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
