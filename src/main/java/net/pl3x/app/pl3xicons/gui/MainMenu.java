package net.pl3x.app.pl3xicons.gui;

import net.pl3x.app.pl3xicons.Main;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class MainMenu extends JMenuBar {
    public MainMenu(Main main) {
        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        // noinspection ConstantConditions
        JMenuItem open = new JMenuItem("Open", new ImageIcon(getClass().getClassLoader().getResource("images/open.png")));
        open.setMnemonic(KeyEvent.VK_O);
        open.setToolTipText("Open icons resource pack");
        open.addActionListener(event -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(new FileNameExtensionFilter("Zip Files", "zip"));
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setCurrentDirectory(main.getLastJarLocation());
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                main.getStatusBar().setStatus("Loading icon pack. Please wait...");
                main.showLoading();
                Main.worker.schedule(() -> {
                    File selectedFile = fileChooser.getSelectedFile();
                    if (main.getResourcePack().openResourcePack(selectedFile.getAbsolutePath())) {
                        main.setPage(main.getResourcePack().getFirstActivePage());
                    }
                    main.setLastJarLocation(selectedFile.getParent());
                    main.hideLoading();
                    main.getStatusBar().setStatus("Ready.");
                }, 50, TimeUnit.MILLISECONDS);
            }
        });

        // noinspection ConstantConditions
        JMenuItem save = new JMenuItem("Save", new ImageIcon(getClass().getClassLoader().getResource("images/save.png")));
        save.setMnemonic(KeyEvent.VK_S);
        save.setToolTipText("Save icons resource pack");
        save.addActionListener(event -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(new FileNameExtensionFilter("Zip Files", "zip"));
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setCurrentDirectory(main.getLastJarLocation());
            File prevFile = main.getResourcePack().getFile();
            if (prevFile != null) {
                fileChooser.setSelectedFile(prevFile);
            }
            if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                main.getStatusBar().setStatus("Saving icon pack. Please wait...");
                File selectedFile = fileChooser.getSelectedFile();
                main.setLastJarLocation(selectedFile.getParent());
                main.getResourcePack().saveResourcePack(selectedFile.getAbsolutePath());
                main.getStatusBar().setStatus("Ready.");
            }
        });

        // noinspection ConstantConditions
        JMenuItem exit = new JMenuItem("Exit", new ImageIcon(getClass().getClassLoader().getResource("images/exit.png")));
        exit.setMnemonic(KeyEvent.VK_E);
        exit.setToolTipText("Exit application");
        exit.addActionListener(event -> System.exit(0));

        file.add(open);
        file.add(save);
        file.add(exit);

        add(file);
    }
}
