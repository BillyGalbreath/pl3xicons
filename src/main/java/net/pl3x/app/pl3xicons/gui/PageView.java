package net.pl3x.app.pl3xicons.gui;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import java.awt.Color;
import java.awt.GridLayout;

public class PageView extends JPanel {
    public PageView() {
        setLayout(new GridLayout(16, 16));
        setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        setBounds(0, 0, 525, 525);
        setBackground(new Color(204, 204, 204));
    }
}
