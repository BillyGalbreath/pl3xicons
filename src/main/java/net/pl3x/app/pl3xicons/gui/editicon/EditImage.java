package net.pl3x.app.pl3xicons.gui.editicon;

import net.pl3x.app.pl3xicons.Main;
import net.pl3x.app.pl3xicons.gui.Icon;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

class EditImage extends JPanel {
    private final EditDialog editDialog;
    private Image alphaBg;
    private BufferedImage currentImage;
    private int width;
    private int height;
    private boolean mouseIsDown;

    EditImage(EditDialog editDialog, Icon icon) {
        this.editDialog = editDialog;

        setImage(icon.getImage());

        EditorMouseAdapter mouseAdapter = new EditorMouseAdapter();
        addMouseListener(mouseAdapter);
        addMouseMotionListener(mouseAdapter);
    }

    void setImage(BufferedImage image) {
        currentImage = image;
        width = currentImage.getWidth();
        height = currentImage.getHeight();

        try {
            alphaBg = ImageIO.read(getClass().getClassLoader().getResourceAsStream("images/alphabg_" + width + ".png"));
        } catch (IOException ignore) {
        }

        setLayout(new GridLayout(width, height, 0, 0));
        setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        setVisible(true);

        updateEditableImage();
    }

    private void updateEditableImage() {
        removeAll();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                add(new Pixel(this, new Color(currentImage.getRGB(x, y), true), x, y));
            }
        }

        revalidate();
        repaint();
    }

    void updateImage(Color color, int x, int y) {
        currentImage.setRGB(x, y, color.getRGB());
        editDialog.setPreviewIcon(currentImage);
    }

    int getIconWidth() {
        return width;
    }

    int getIconHeight() {
        return height;
    }

    private boolean isMouseDown() {
        return mouseIsDown;
    }

    private void setMouseDown(boolean mouseIsDown) {
        this.mouseIsDown = mouseIsDown;
    }

    private EditDialog getEditDialog() {
        return editDialog;
    }

    private Color getCurrentColor() {
        return getEditDialog().getDrawingTools().getColorPicker().getSelectionModel().getSelectedColor();
    }

    private void setCurrentColor(Color color) {
        getEditDialog().getDrawingTools().getColorPicker().getSelectionModel().setSelectedColor(color);
    }

    private void redraw() {
        Main.worker.schedule(() -> {
            revalidate();
            repaint();
        }, 50, TimeUnit.MILLISECONDS);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (alphaBg == null) {
            return;
        }
        int w = alphaBg.getWidth(this);
        int h = alphaBg.getHeight(this);
        if (w <= 0 || h <= 0) {
            return;
        }
        for (int x = 2; x <= getWidth(); x += w) {
            for (int y = 2; y <= getHeight(); y += h) {
                g.drawImage(alphaBg, x, y, w, h, this);
            }
        }
    }

    private class EditorMouseAdapter extends MouseAdapter {
        private Pixel currentPixel;
        private Pixel previousPixel;

        @Override
        public void mousePressed(MouseEvent event) {
            if (getEditDialog().isAdvancedMode()) {
                setMouseDown(true);

                Pixel pixel = getPixel(event);
                if (pixel == null) {
                    return;
                }

                doTool(pixel);
            }
        }

        @Override
        public void mouseReleased(MouseEvent event) {
            if (getEditDialog().isAdvancedMode()) {
                setMouseDown(false);

                Pixel pixel = getPixel(event);
                if (pixel == null) {
                    return;
                }

                doTool(pixel);
                highlightTool(pixel);
            }
        }

        @Override
        public void mouseMoved(MouseEvent event) {
            if (getEditDialog().isAdvancedMode()) {
                Pixel pixel = getPixel(event);
                if (pixel != currentPixel) {
                    previousPixel = currentPixel;
                    currentPixel = pixel;

                    if (previousPixel != null) {
                        previousPixel.setBackground(previousPixel.getColor());
                        previousPixel.revalidate();
                    }
                }
                if (pixel == null) {
                    return;
                }

                if (isMouseDown()) {
                    doTool(pixel);
                } else {
                    highlightTool(pixel);
                }
            }
        }

        @Override
        public void mouseDragged(MouseEvent event) {
            mouseMoved(event);
        }

        @Override
        public void mouseExited(MouseEvent event) {
            if (currentPixel != null) {
                currentPixel.setBackground(currentPixel.getColor());
                currentPixel.revalidate();
            }
            if (previousPixel != null) {
                previousPixel.setBackground(previousPixel.getColor());
                previousPixel.revalidate();
            }
            redraw();
            previousPixel = null;
            currentPixel = null;
        }

        private void doTool(Pixel pixel) {
            switch (getEditDialog().getDrawingTools().getColorPickerTools().getToolType()) {
                case DROPPER:
                    setCurrentColor(pixel.getColor());
                    pixel.invertColor();
                    break;
                case ERASER:
                    pixel.setColor(new Color(0, 0, 0, 0));
                    pixel.setBackground(Color.PINK);
                    break;
                case PENCIL:
                default:
                    pixel.setColor(getCurrentColor());
            }
            pixel.revalidate();
            redraw();
        }

        private void highlightTool(Pixel pixel) {
            switch (getEditDialog().getDrawingTools().getColorPickerTools().getToolType()) {
                case DROPPER:
                    pixel.invertColor();
                    break;
                case ERASER:
                    pixel.setBackground(Color.PINK);
                    break;
                case PENCIL:
                default:
                    pixel.setBackground(getCurrentColor());
            }
            pixel.revalidate();
            redraw();
        }

        private Pixel getPixel(MouseEvent event) {
            Component component = ((JComponent) event.getSource()).getComponentAt(event.getPoint());
            if (component instanceof Pixel) {
                return (Pixel) component;
            }
            return null;
        }
    }
}
