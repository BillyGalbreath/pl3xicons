package net.pl3x.app.pl3xicons.gui.colorpicker;

import javax.swing.JColorChooser;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

public class ColorPicker extends JColorChooser {
    ColorPicker() {
        setColor(Color.BLACK);

        for (Component component : getComponents()) {
            if (component.getName().equals("ColorChooser.previewPanelHolder")) {
                remove(component);
            }
        }
    }

    @Override
    public void setChooserPanels(AbstractColorChooserPanel[] panels) {
        AbstractColorChooserPanel[] newPanels = new AbstractColorChooserPanel[1];
        for (AbstractColorChooserPanel panel : panels) {
            if (panel.getDisplayName().equals("HSL")) {
                newPanels[0] = panel;
            }
        }
        super.setChooserPanels(newPanels);
    }

    @Override
    public void paint(Graphics g) {
        // fix z-order drawing
        super.paint(g);
        getParent().repaint();
    }
}
