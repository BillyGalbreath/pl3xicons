package net.pl3x.app.pl3xicons.gui.colorpicker;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

class ColorPreviewHolder extends JPanel {
    private Image alphaBg;

    ColorPreviewHolder() {
        try {
            alphaBg = ImageIO.read(getClass().getClassLoader().getResourceAsStream("images/alphabg_16.png"));
        } catch (IOException ignore) {
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (alphaBg == null) {
            return;
        }
        int w = alphaBg.getWidth(this);
        int h = alphaBg.getHeight(this);
        if (w <= 0 || h <= 0) {
            return;
        }
        for (int x = 1; x < getWidth(); x += w) {
            for (int y = 1; y < getHeight(); y += h) {
                g.drawImage(alphaBg, x, y, w, h, this);
            }
        }
    }
}
