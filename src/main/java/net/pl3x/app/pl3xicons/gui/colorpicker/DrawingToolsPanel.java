package net.pl3x.app.pl3xicons.gui.colorpicker;

import javax.swing.JLayeredPane;
import javax.swing.border.EtchedBorder;
import javax.swing.colorchooser.ColorSelectionModel;

public class DrawingToolsPanel extends JLayeredPane {
    private final ColorPickerTools colorPickerTools;
    private final ColorPicker colorPicker;

    public DrawingToolsPanel() {
        setLayout(null);
        setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        setBounds(10, 275, 626, 195);

        colorPicker = new ColorPicker();
        colorPicker.setBounds(1, 1, 624, 193);
        add(colorPicker);

        colorPickerTools = new ColorPickerTools();
        add(colorPickerTools);

        ColorSelectionModel model = colorPicker.getSelectionModel();
        model.addChangeListener(e -> colorPickerTools.setColor(model.getSelectedColor()));

        colorPickerTools.setColor(colorPicker.getSelectionModel().getSelectedColor());

        setLayer(colorPickerTools, 1);
    }

    public ColorPicker getColorPicker() {
        return colorPicker;
    }

    public ColorPickerTools getColorPickerTools() {
        return colorPickerTools;
    }
}
