package net.pl3x.app.pl3xicons;

import net.pl3x.app.pl3xicons.gui.Icon;
import net.pl3x.app.pl3xicons.gui.MainMenu;
import net.pl3x.app.pl3xicons.gui.PageControls;
import net.pl3x.app.pl3xicons.gui.PageView;
import net.pl3x.app.pl3xicons.gui.StatusBar;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import java.awt.Dimension;
import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Main extends JFrame {
    public static final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();

    private final JLabel loading;
    private final PageView pageView;
    private final PageControls pageControls;
    private final ResourcePack resourcePack;
    private final StatusBar statusBar;

    private File lastJarLocation;
    private File lastImageLocation;

    private String page = "00";

    private Main() {
        String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        setLastJarLocation(path);
        setLastImageLocation(path);

        resourcePack = new ResourcePack(this);

        setTitle("Pl3xIcons Editor");
        setType(Type.NORMAL);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setIconImages(Logo.getIcons());
        setMinimumSize(new Dimension(532, 640));
        getContentPane().setLayout(null);

        setJMenuBar(new MainMenu(this));

        loading = new JLabel();
        loading.setBounds(0, 0, 525, 525);
        //noinspection ConstantConditions
        loading.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/loading.gif")));
        loading.setHorizontalAlignment(JLabel.CENTER);
        loading.setVerticalAlignment(JLabel.CENTER);
        loading.setVisible(false);
        getContentPane().add(loading);

        pageView = new PageView();
        getContentPane().add(pageView);

        pageControls = new PageControls(this);
        getContentPane().add(pageControls);

        statusBar = new StatusBar();
        getContentPane().add(statusBar);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void showLoading() {
        loading.setVisible(true);
        loading.revalidate();
        pageView.removeAll();
        pageView.revalidate();
        repaint();
    }

    public void hideLoading() {
        loading.setVisible(false);
        loading.revalidate();
        pageView.revalidate();
        repaint();
    }

    public ResourcePack getResourcePack() {
        return resourcePack;
    }

    public StatusBar getStatusBar() {
        return statusBar;
    }

    public void setPage(String page) {
        this.page = page;

        pageView.removeAll();
        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                String hex = page + Integer.toHexString(y) + Integer.toHexString(x);
                Icon icon = getResourcePack().getIcon(hex);
                pageView.add(icon);
            }
        }
        pageView.revalidate();
        pageView.repaint();

        pageControls.setPage(page);
    }

    public String getPage() {
        return page;
    }

    public File getLastJarLocation() {
        return lastJarLocation;
    }

    public void setLastJarLocation(String location) {
        lastJarLocation = new File(location);
    }

    public File getLastImageLocation() {
        return lastImageLocation;
    }

    public void setLastImageLocation(String location) {
        lastImageLocation = new File(location);
    }

    public static void main(String[] args) {
        new Main();
    }
}
